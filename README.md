# Wireless camera monitor

This is my wireless camera monitor based on a cheap HDMI transmitter and USB-C monitor.

# Description

## Use Case
The use cases for video monitoring solutions are plentiful. From checking the frame when filming yourself to see if everything is alright, to viewing a camera feed when the camera is not accessible. The benefits are endless. Even just being able to show what your filming to people who are not behind the camera can make it worth to invest in a monitoring solution. Now, you can either pull cables across your set and create a tripping hazard, or get yourself a wireless transmitter. Because commercial solutions start at multiple hundred euros, I decided to create my own.

## Things I tried
I first started with the built in camera WIFI feed. My Canon cameras have the ability to stream the video feed over WIFI; I’m sure other brands have this feature as well. With my cameras I’ve never got it working quite right. I always had problems either getting a connection or keeping the connection up. And when the connection was steady, framerate, reaction time and quality were not great. Sometimes the image would freeze and then continue a few seconds later, or not at all. Lowering the resolution of the transmission helped, but not a lot.
This was the solution I had used for years and I was fed up enough to try something new.
For my livestreams I have used HDMI to USB dongles to get a camera feed to OBS Studio. These dongles are relatively cheap and work reliably for hours. You just use the HDMI-out of your camera and connect it with an HDMI cable to this dongle. The dongle registers as a webcam on your computer and from there you can use it in any program you like.
My Idea here was to hook up a dongle to the USB on-the-go interface of a Raspberry Pi Zero W and stream the incoming video feed over WIFI. This worked with a reliable connection, but the delay of about 3 seconds made it unusable. Even switching to the more powerful Raspberry Pi Zero W 2 made no difference.
A more robust solution was needed.

## HDMI Transmitter
I’ve had my eyes on cheap wireless HDMI transmitters for a while. Although more expensive than the solutions I had tried, they were still cheaper than commercial solutions built for this purpose. During a sale on Amazon I pulled the trigger on a wireless transmitter receiver pair. I chose a pair with a dedicated USB and HDMI socket instead of a dongle style USB stick because I planned to power everything with a Power-bank.
After the modules arrived, I tested them and was surprised with the quality of the feed. The signal is in full HD with less than 200 milliseconds of delay. No frame freezes, no interruptions, it just worked out of the box. All I needed then was something to show the feed on.
Here I used my trusted HDMI to USB dongle again so I could just use the windows camera application and view my feed this way. This worked great and I could have stopped here. However, I wanted a fully mobile solution with no dangling wires and without having to take away my laptop from the workbench, because I want to be able to use it in my shots.

## USB-C Monitor
A while ago I got myself a USB-C monitor for my laptop as a secondary screen when I travel. It only needs a USB-C connection for signal and power, and it is very light and thin. Alternatively, you can power it via USB-C and use HDMI as an input. This was perfect for my purpose.

## Power-bank
To power it all, I chose a beefy 25.000mAh Power-bank. After four hours of continuous use, this beast of a battery still had 75% charge. Nice.

## Case
All that was left to do, was to tie it all together in a more compact form. The cage I designed holds on to the four corners of the monitor and houses the receiver and power-bank on the backside. The power-bank weighs everything down, so the whole thing can be placed anywhere without falling over. Additionally I’ve added VESA 75 and VESA 100 hole patterns, as well as two 1/4 inch 20 inserts in the middle to mount it to almost anything. I have mounted my setup to an arca swiss plate on the back of the monitor. The corresponding clamp is mounted to an articulating vesa monitor arm. This makes it really easy to mount and unmount the monitor.
I have created exact models for my power bank and receiver to be able to snap fit them into my design, which worked perfectly after four iterations or so. The transmitter snaps into a similar holder on the back of my camera monitor.
All parts print in sections to fit smaller build-plates and to make iterating easier, because you don’t have to scrap the whole thing to make minor changes. Also, it is easier for you to adapt to a specific monitor or receiver in case you can not buy the exact models I have.

## Use
I use this solution for everything I shoot now. It is fast to setup, the battery lasts forever and as a nice side effect, I can monitor my audio remotely! That’s right, the camera audio is streamed to HDMI, in and out of my on camera monitor, through the transmitter and receiver and into the USB-monitor. The monitor itself has crappy built in speakers, but also an audio jack for headphones. This is also great for when my awesome wife helps me shoot videos and I don’t have to worry about the audio because she is so awesome and cool and did not force me to write this.

## Conclusion
So, is this the solution that doctors don’t want you to know about and you can make 2000$ from home with it?
No.
While being a practical and relatively cheap solution, it definitely has some problems. Because everything is either cheap plastic or 3D printed, this setup is too fragile for a rough outdoor set. The monitor is relatively bright, but too dim for the outside as well. Also transmission range is limited. As long as transmitter and receiver are in the same room it works really well, but as soon as there is more than one wall or ceiling in between, the connection gets spotty.

# Raspberry pi
For the setup with the raspberry pi zero w 2 I have used this commandline
```
ffmpeg -r 30 -i /dev/video0 -aspect 16:9 -listen 1 -c:v mpeg2video -r 30 -f mpegts http://raspberrypi.local:8080
```

If not working, try this first. (why????)
```
ffmpeg -r 10 -video_size 640x480 -i /dev/video0 -listen 1 -c:v mpeg2video -r 10 -f mpegts udp://raspberrypi.local:12345
```

Maybe you are more lucky than I was ;)

# Used hardware

**INIU Power Bank, 100W 25000mAh** https://www.amazon.de/gp/product/B0B7438Q2Z/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

**COOLHOOD Portable Monitor, 15,6 Zoll** https://www.amazon.de/gp/product/B0CLNMTSHY/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

**Wireless HDMI Transmitter and Receiver, Binken Wireless HDMI Extender Kit** https://www.amazon.de/gp/product/B0BMB5MRBG/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

## Screws
All screws that hold the cage together are M3 screws with M3 heat inserts. The inserts in the four corner pieces are the short type because of limited space. 
The VESA mount uses M4 screws and inserts.
The 1/4 20 inserts use, well, 1/4 20 screws.

## Printables
https://www.printables.com/model/905017-wireless-hdmi-transmitter